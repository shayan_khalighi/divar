import React from "react";
import { Carousel } from "react-responsive-carousel";

export default () => (
  <Carousel>
  
    <div>
      <img alt="" src="/images/1.jpg" />
    
    </div>
    <div>
      <img alt="" src="/images/2.jpg "/>
    
    </div>
    <div>
      <img alt="" src="/images/3.jpg" />
    
    </div>
    
    <div>
      <img alt="" src="/images/4.jpg" />
      
    </div>
    <div>
      <img alt="" src="/images/5.jpg" />
      
    </div>
    <div>
      <img alt="" src="/images/6.jpg" />
      
    </div>
   
  </Carousel>
);
