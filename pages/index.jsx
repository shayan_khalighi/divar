import Head from "next/head";
import Link from "next/link";
import Carousel from "./slider";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import Slider from "./agahiSlider";
import { SocialIcon } from "react-social-icons";

export default function Home() {
  return (
    <div className="mb-2 ">
      <Head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <title>DIVAR</title>
      </Head>

      <nav className="shadow-md fixed z-50 bg-white w-screen border border-b-gray mb-8 md:visible  invisible">
        <ul className=" flex  justify-between ">
          <ul className="flex mt-2 ml-4">
            <li className="mr-6 p-2">
              <a
                className="text-white px-4 py-2 rounded-md text-sm"
                href="#"
                style={{ backgroundColor: "#a62626" }}
              >
                ثبت آگهی
              </a>
            </li>
            <li className="mr-6 pt-2">
              <a className="text-xs" href="#" style={{ color: "#707070" }}>
                خروج
              </a>
            </li>
            <li className="mr-6 p-2">
              <a style={{ color: "#707070" }} className="text-xs" href="#">
                پشتیبانی
              </a>
            </li>
            <li className="mr-6 p-2">
              <a style={{ color: "#707070" }} className="text-xs" href="#">
                بلاگ
              </a>
            </li>
            <li className="mr-6 p-2">
              <a style={{ color: "#707070" }} className="text-xs" href="#">
                درباره دیوار
              </a>
            </li>
            <li className="mr-6 p-2">
              <a style={{ color: "#707070" }} className="text-xs" href="#">
                چت
              </a>
            </li>
            <li className="mr-6 p-2">
              <a style={{ color: "#707070" }} className="text-xs" href="#">
                دیوار من
              </a>
            </li>
          </ul>
          <ul className="flex  justify-between ">
            <li className="p-2 pr-2">
              <a
                style={{ color: "#707070" }}
                className="flex mt-3 text-sm"
                href="/home"
              >
                انتخاب شهر{" "}
                <svg
                  className="h-4 w-4 text-black ml-1 "
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z"
                  />
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M15 11a3 3 0 11-6 0 3 3 0 016 0z"
                  />
                </svg>
              </a>
            </li>
            <li className="pr-4">
              <a href="/home">
                <img className="h-14" src="/images/logo.png" alt="دیوار" />
              </a>
            </li>
          </ul>
        </ul>
      </nav>
      <nav className="shadow-md w-z fixed inset-0 z-50 border border-gray h-16  bg-white md:invisible visible">
        <ul className="flex justify-between mt-4 ">
          <ul className="flex ">
            <li>
              <svg
                className="h-5 w-5  ml-4 mr-2 mt-2"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                strokeWidth="2"
                stroke="currentColor"
                fill="none"
                strokeLinecap="round"
                strokeLinejoin="round"
                style={{ color: "#707070" }}
              >
                {" "}
                <path stroke="none" d="M0 0h24v24H0z" />{" "}
                <circle cx="6" cy="12" r="3" /> <circle cx="18" cy="6" r="3" />{" "}
                <circle cx="18" cy="18" r="3" />{" "}
                <line x1="8.7" y1="10.7" x2="15.3" y2="7.3" />{" "}
                <line x1="8.7" y1="13.3" x2="15.3" y2="16.7" />
              </svg>
            </li>
            <li>
              <svg
                className="h-6 w-6 mt-1.5"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                strokeWidth="2"
                stroke="currentColor"
                fill="none"
                strokeLinecap="round"
                strokeLinejoin="round"
                style={{ color: "#707070" }}
              >
                {" "}
                <path stroke="none" d="M0 0h24v24H0z" />{" "}
                <path d="M16 6h3a1 1 0 0 1 1 1v11a2 2 0 0 1 -4 0v-13a1 1 0 0 0 -1 -1h-10a1 1 0 0 0 -1 1v12a3 3 0 0 0 3 3h11" />{" "}
                <line x1="8" y1="8" x2="12" y2="8" />{" "}
                <line x1="8" y1="12" x2="12" y2="12" />{" "}
                <line x1="8" y1="16" x2="12" y2="16" />
              </svg>
            </li>
            <li>
              <svg
                className="h-5 w-5  ml-2 mt-2"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                style={{ color: "#707070" }}
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z"
                />
              </svg>
            </li>
          </ul>
          <ul>
            <li>
              <svg
                className="h-8 w-8 mt-0.5 mr-2"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                strokeWidth="2"
                stroke="currentColor"
                fill="none"
                strokeLinecap="round"
                strokeLinejoin="round"
                style={{ color: "#707070" }}
              >
                {" "}
                <path stroke="none" d="M0 0h24v24H0z" />{" "}
                <line x1="5" y1="12" x2="19" y2="12" />{" "}
                <line x1="15" y1="16" x2="19" y2="12" />{" "}
                <line x1="15" y1="8" x2="19" y2="12" />
              </svg>
            </li>
          </ul>
        </ul>
      </nav>
      <section className="w-full z-30 pt-20 lg:grid lg:grid-cols-9 xl:grid-cols-10 flex flex-col justify-center px-4">
        <div
          className="col-start-3 col-span-6 text-right  text-xs"
          style={{ color: "#707070" }}
        >
          وسایل نقلیه{" > "}
          خودرو{" > "}
          سواری{" > "}
          نیسان{" > "}
          پاترول 4 در{" > "}4 سیلندر{" > "}
          نیسان پاترول 4 در 4 سیلندر، مدل ۱۳۷۸
        </div>
        <div className="lg:col-start-1  lg:col-end-9 xl:col-start-2  lg:grid lg:grid-cols-10 lg:gap-20 mt-12   lg:ml-0  ">
          <div className="col-start-2 col-span-6 text-right   ">
            <div className="w-full  pb-2">
              <Carousel className="rounded" />
            </div>
            <div className="md:visible  invisible absolute md:static">
              <textarea
                rows="5"
                type="text"
                className="rounded-md border text-right w-full h-500"
                placeholder="...یادداشت شما"
              />
              <span
                className="font-extralight  text-xs mr-2 text-xs"
                style={{ color: "#707070" }}
              >
                .یادداشت تنها برای شما قابل دیدن است و پس از حذف آگهی، پاک خواهد
                شد
              </span>
            </div>
            <div className="border rounded mb-8 mt-4 md:visible  invisible absolute md:static px-2">
              <div className="flex justify-between border-b-2">
                <img
                  className="h-20 ml-4"
                  src="/images/karnameh-logo.png"
                  alt="karnameh"
                />
                <p className="p-4">
                  <span className="text-xl " style={{ color: "#212121" }}>
                    کارشناسی با کارنامه
                  </span>{" "}
                  <br />
                  <span
                    className="text-sm font-light "
                    style={{ color: "#707070" }}
                  >
                    با هزینهٔ ۲۵۰٬۰۰۰ تومان
                  </span>
                </p>
              </div>
              <div
                className="text-sm font-light mx-4 mt-4"
                style={{ color: "#707070" }}
              >
                کارشناسان کارنامه با تجهیزات کامل و از طرف شما این خودرو را
                کارشناسی می‌کنند و گزارش آن را برای شما می‌فرستند
              </div>
              <button
                className="  w-full text-base  border  rounded h-10 w-64 mr-2 my-4"
                style={{ borderColor: "#a62626", color: "#a62626" }}
              >
                اطلاعات بیشتر و درخواست کارشناسی
              </button>
            </div>
            <div
              className="flex justify-end font-light border-b-2 pb-4 md:visible  invisible absolute md:static text-sm "
              style={{ color: "#707070" }}
            >
              {" "}
              راهنمای خرید امن
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6 ml-1"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  fillRule="evenodd"
                  d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-8-3a1 1 0 00-.867.5 1 1 0 11-1.731-1A3 3 0 0113 8a3.001 3.001 0 01-2 2.83V11a1 1 0 11-2 0v-1a1 1 0 011-1 1 1 0 100-2zm0 8a1 1 0 100-2 1 1 0 000 2z"
                  clipRule="evenodd"
                />
              </svg>
            </div>
            <div
              className="flex justify-end font-light border-b-2 py-4 md:visible  invisible absolute md:static text-sm"
              style={{ color: "#707070" }}
            >
              گزارش مشکل آگهی
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6 ml-1"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  fillRule="evenodd"
                  d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
                  clipRule="evenodd"
                />
              </svg>
            </div>
          </div>
          <div className="col-start-8 col-span-6 text-right  ">
            <div className="mt-4 leading-10">
              <span
                className="font-bold  mt-4 text-2xl"
                style={{ color: "#212121" }}
              >
                نیسان پاترول 4 در 4 سیلندر، مدل ۱۳۷۸
              </span>
              <br />{" "}
              <span className=" text-base" style={{ color: "#707070" }}>
                لحظاتی پیش در تهران، استخر | سواری
              </span>
            </div>
            <div className="flex md:justify-between lg:flex-col-reverse justify-col  flex-col-reverse md:flex-row mt-8 md:visible  invisible absolute md:static">
              <div className="flex justify-between md:justify-start lg:justify-between ml-10 mr-12 mt-8 md:mt-3">
                <svg
                  className="h-5 w-5  ml-4 mr-4"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  strokeWidth="2"
                  stroke="currentColor"
                  fill="none"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  style={{ color: "#707070" }}
                >
                  {" "}
                  <path stroke="none" d="M0 0h24v24H0z" />{" "}
                  <circle cx="6" cy="12" r="3" />{" "}
                  <circle cx="18" cy="6" r="3" />{" "}
                  <circle cx="18" cy="18" r="3" />{" "}
                  <line x1="8.7" y1="10.7" x2="15.3" y2="7.3" />{" "}
                  <line x1="8.7" y1="13.3" x2="15.3" y2="16.7" />
                </svg>
                <svg
                  className="h-5 w-5  ml-2"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  style={{ color: "#707070" }}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z"
                  />
                </svg>
              </div>
              <div className="flex justify-between mr-2">
                <button
                  style={{ color: "#707070", borderColor: "#858585" }}
                  className=" text-base border  h-8 w-40 rounded  mr-2 text-gray-500"
                >
                  چت
                </button>
                <button
                  className="  text-base  text-white h-8 w-40 rounded "
                  style={{ backgroundColor: "#a62626" }}
                >
                  اطلاعات تماس
                </button>
              </div>
            </div>
            <div className="grid grid-col-6 gap-2 text-center mt-8">
              <div className="col-start-1 col-span-2   ">
                <span
                  className="font-hairline text-sm"
                  style={{ color: "#707070" }}
                >
                  رنگ
                </span>{" "}
                <br />
                <span
                  className="font-medium text-lg "
                  style={{ color: "#212121" }}
                >
                  سبز
                </span>
              </div>
              <div className="col-start-3 col-span-2 border-l-2 border-r-2  ">
                <span
                  className="font-hairline  text-sm"
                  style={{ color: "#707070" }}
                >
                  سال ساخت
                </span>{" "}
                <br />
                <span
                  className="font-medium text-lg "
                  style={{ color: "#212121" }}
                >
                  ۱۳۷۸
                </span>
              </div>
              <div className="col-start-5 col-span-2 ">
                <span
                  className="font-hairline text-sm"
                  style={{ color: "#707070" }}
                >
                  کارکرد
                </span>{" "}
                <br />
                <span
                  className="font-medium text-lg "
                  style={{ color: "#212121" }}
                >
                  ۳۰۰,۰۰۰
                </span>
              </div>
            </div>
            <div className="text-base">
              <div className="flex justify-between border-t-2 mt-6 p-3 ">
                <a href="#" style={{ color: "#a62626" }}>
                  نیسان، پاترول 4 در، 4 سیلندر
                </a>
                <span style={{ color: "#707070" }}>برند و مدل</span>
              </div>
              <div className="flex justify-between border-t-2  p-3">
                <span style={{ color: "#212121" }}>سالم</span>
                <span style={{ color: "#707070" }}>وضعیت موتور</span>
              </div>
              <div className="flex justify-between border-t-2  p-3">
                <span style={{ color: "#212121" }}>۳ ماه</span>
                <span style={{ color: "#707070" }}>مهلت بیمهٔ شخص ثالث</span>
              </div>
              <div className="flex justify-between border-t-2  p-3">
                <span style={{ color: "#212121" }}>دنده‌ای</span>
                <span style={{ color: "#707070" }}>گیربکس</span>
              </div>
              <div className="flex justify-between border-t-2  p-3">
                <span style={{ color: "#212121" }}>تک برگی</span>
                <span style={{ color: "#707070" }}>سند</span>
              </div>
              <div className="flex justify-between border-t-2  p-3">
                <span style={{ color: "#212121" }}>نقدی</span>
                <span style={{ color: "#707070" }}>نحوه فروش</span>
              </div>
              <div className="flex justify-between border-t-2 border-b-2  p-3 ">
                <div>
                  <span style={{ color: "#212121" }}>۱۴۵٫۰۰۰٫۰۰۰</span>
                  <span style={{ color: "#212121" }}>تومان</span>
                </div>

                <span style={{ color: "#707070" }}>قیمت</span>
              </div>
            </div>
            <div
              className="mt-8 space-y-8 leading-relaxed "
              style={{ color: "#212121" }}
            >
              <span className="text-xl font-black "> توضیحات</span>
              <br />
              <br />
              <span className="font-thin text-base ">
                پاترول چهار در چهار سیلندر
              </span>
              <br />
              <span className="font-thin text-base ">فنی عالی</span>
              <br />
              <span className="font-thin text-base ">
                تودوزی پاجیرویی و کنسول وسط
              </span>
              <br />
              <span className="font-thin text-base  ">لاستیک جلو صد درصد</span>
              <br />
              <span className="font-thin text-base ">
                اطاق تعویض قید در سند
              </span>
              <br />
            </div>
            <div className="mt-8 mb-8">
              <button
                className=" bg-gray-100 px-3 py-1 text-sm"
                style={{ color: "#707070" }}
              >
                سواری در استخر
              </button>
              <button
                className=" bg-gray-100 ml-2 px-3 py-1 text-sm"
                style={{ color: "#707070" }}
              >
                سواری
              </button>
            </div>
            <div className="visible  md:invisible md:absolute static text-right">
              <textarea
                rows="5"
                type="text"
                className="rounded-md border text-right w-full h-500"
                placeholder="...یادداشت شما"
              />
              <span
                className="font-extralight text-xs"
                style={{ color: "#707070" }}
              >
                .یادداشت تنها برای شما قابل دیدن است و پس از حذف آگهی، پاک خواهد
                شد
              </span>
            </div>
          </div>
          <div
            style={{ color: "#707070" }}
            className="flex justify-end  font-light border-b-2 pb-4 mt-4 visible  md:invisible md:absolute static text-right text-sm "
          >
            {" "}
            راهنمای خرید امن
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-6 w-6 ml-1"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fillRule="evenodd"
                d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-8-3a1 1 0 00-.867.5 1 1 0 11-1.731-1A3 3 0 0113 8a3.001 3.001 0 01-2 2.83V11a1 1 0 11-2 0v-1a1 1 0 011-1 1 1 0 100-2zm0 8a1 1 0 100-2 1 1 0 000 2z"
                clipRule="evenodd"
              />
            </svg>
          </div>
          <div
            className="flex justify-end font-light border-b-2 py-4 visible  md:invisible md:absolute static text-sm"
            style={{ color: "#707070" }}
          >
            گزارش مشکل آگهی
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-6 w-6 ml-1"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fillRule="evenodd"
                d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
                clipRule="evenodd"
              />
            </svg>
          </div>
        </div>
      </section>
      <section className=" object-cover lg:mr-16 mr-4 lg:w-200 mt-12 overflow-hidden lg:grid lg:grid-cols-9 xl:grid-cols-10">
        <div className=" text-right lg:col-start-1  lg:col-end-9 xl:col-start-2 ">
          <span className="font-semibold text-lg" style={{ color: "#212121" }}>
            آگهی‌های مشابه
          </span>
          <Slider className="flex flex-nowrap rounded " />
        </div>
      </section>
      <footer className="pt-28 mb-2 ">
        <ul className="flex flex-col-reverse justify-center lg:flex-row-reverse lg:justify-between lg:grid lg:grid-cols-9">
          <ul className="flex lg:col-start-3 pl-4 justify-center mr-6 mb-12 md:mb-2">
            <li>
              <a href="#">
                <img
                  className="h-6 w-6 mt-1.5"
                  alt="Aparat"
                  src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAAIpklEQVRoge1ZfYwUZxn/PTNzw22B3pUCtVaqUikNFzjZeedue1ypV2kRSAmKoVWgwVRIm0YFUwXTWqiSKB9VgylNa2qlFWrA2ognH1K72F7XvZuZhUWvKa5QCyQWUHq5oge7s/P4x+4ss3Ozs8vl2vpHf3/t8/E+7++Z93k/F/gQHyzochvE43Fl9OjRGjNPLKqONTY2pidNmnRxmLnVhJoT2Llzpzxx4sQHAKwG8FGf+SIAk4heA9DV399/oKOj48Iw8qyImhIwTfMKIvo1M88JMGcBvARgPzO/SkRHhRD/HVaWIVBq9HsqgPzbAH5SV1f3THNz85lh5lUzqo5AT0/PrZIkHfSosgA2RiKRDU1NTeffO2q1oeoISJK03CO+zsxLdF0/5PdLp9Pjs9nsZyVJmsXMTQA+BmA8CvPjPDOfIiKDiHqYuVMI8S9/jFQqNcVxnHuI6KjjOAld149W4xc6AsxMlmWdBjAOwIELFy4sbG9vf9fX6c2O46wC8HnUXpI2M++VZXlzNBp9xWvo6en5iCzLLzBzG4CzRLQPwM80TXv1shPo7u6+UZblowBejEQidzc1NWVd2+HDh6/L5/NbmXl+jaQrYQ+ArwshjrmKRCIRUVV1B4AFHr9fNDQ03OdfrqWwyIqizADwWjabXewlbxjG3bZt9w4DeQCYC+CQZVlLXEVbW9tAJBK5i4he8vgt6+vr28XMZZxDE2DmG23bXtjW1jbgIf9tItoBoGEYyLsYzczPGYaxzlU0NTVlc7ncQgBvuDoiujOVSt3vbRiagCRJz8VisdOubJrmI0S0AcGldxzADgDPAxjSskpEay3LWuvKsVisH8BiFFY+AAAzr/GOQs07sWEYC4loV0AbB8Cq48ePP75o0aI8AMTj8fpRo0ZtJaKvDCURZv6yruvPu7JlWeuZ+aESaSKhaZoFVBkBF4cOHfoEEW0LIA8AG4QQW1zyANDR0XHh/PnzKwAcDPCvCiJ6qru7+5OunMvlNqJ8VG9wf9SUQD6f3wRgZIDJAfCjoDYdHR02M2+qEvqdYgw/Rsmy/FNXiMVi/cy8xWMvfciqCaRSqZkAvljBfCZoQyr1QhS0EQ0A+AaAkUKIMaqqjmXmRzE4kXmGYcxwBWZ+GkAOABzHebPmBBzHeSDEPCaRSEQqGZl5QoB6gRBii3vgmzZt2ju6rq9j5u/4HYnoW+7vlpaWtwG8DOBiY2NjuqYEjhw5chWAsLVeVVX1q5WMROS37RdC/CHIVwixCcDffOq56XR6vCsw834ApnczC00gl8stAFAf5gNgs2EYt3sVzEzFNX2xT/9ypSBExAASPnVdLpcrnYIlSXqleOcoIfTswswzq5AHAJWI9lmW1ek4Tg8RqaZpziUiEUByRJVYQfbPANgGAIqivGHbdpfXWO3w1VzF7kJi5vlENL9ItJLfPGZeX/zaZSjOpVkBbaaWyDQ3/yeTyZSVYLVJ/PEq9jDssW17gm3bE4hob1HXalnWw37HeDyuqKq6FYVTrx/XewX/YS50JzZNM4faj8hlsG17QiwWOwUAPT09EyRJOlE0MRH9Mp/PP01EJyVJmgLgQWa+tUKorBCiYulVIxeWYJyIfkxEx2zbHifL8r3MvMRtI0lSpbbEzEslSVoKAMyDqsmPoI2uhGoJ9AG4ehADop9Ho9HlROQN/ifTNDMAvgcAsiw/mUwmVyiKIjHzk9VYhqA/zFg2BzKZjH+oTmAwzg0MDKz0kQcAaJq2HsBfAICZ5yiKchLAW0T0ucvjXIZ/hBnLEujv77/DNM0rXJmI/hrQZp//WunxZyLaXaGvNID1zPxw0Se0NDw4Emb0l1A7M58C4F7a4wCW+kiGPlgx86CXCmZeLYTY5F0+DcO4nYg6Aahh8fwblx9lI8DMM4joFlfO5XJ7ANh+H2auOLmLl3Evdum6vtG/9uu6foCZ14SRA5Czbft3NSVQrH8BYLari8Vip5l5r6/NZNM0VwQFsyxrNhHN8+qYeWulzh3HeRa+D+RDZ2tr679D7JcS6Ovra0ZhK5+VTCavcfVEtNHfiIgeNwxjXSKRGAMApmmONU3zm8y8G75RdRznrUqdF8mdq2Rn5sfCyJclgEu3HFVRlNJVUAjRBeD3vnYyEa1VVfWsaZrnAPwTwGMIqGdFUSZX6rz4AcZWMP9W1/XQ+vcn4K3RVV1dXaNdIZ/Pfw1A0DOiBOAqhOwnzLyq0pxRVXUlgo8z/bIsrwwj7iUAAJBl+e8e/fhIJLLaFVpbW99k5sC6rwF3WJa1NR6Pl47lzEymaa4G8FCAPzPzfdOnTw9d/12UvgwzS5ZlnQBwXVGVBXCzECLl+liWtZaZ12FoOAPgjyi8lbYD+FQFv0eEEN93hVQqNSUajb5eKeil9xUih4h+4LGpALYnk8krXYWmaY8W769DwXgAXwKwLIT8d73kTdO81nGce8KCltVfNBp9AuUT9qa6uroXent7S5NT1/V1RLQEQOBuPES8S0R3CSHWu4ri/eA3FR4GSihLgIichoaGhSjegACAmWcNDAzs9F7eNU3bzsyfBtA5DOQ7JUmaqmnaTlfR29urjhgx4lcAYrZth65EFXdU0zTbASwHMAfAOGb+MzN/ofg64Pd7EIVH2roaSecAdDqOs7mlpaXsHpxMJq9UFOVFALcBOKtp2jVBN7iqCXhhGMZkSZLamHmyJEnPBk2q7u7uqxVFuRPALQCmMvP1KDwAKyhsVieZ+QiAg7Is741Go2f9MSzL0ph5OwB379gmhFgWxu2y/2Z9L1D86mtQGEl3FNlxnHb/CPkxpOvicME0zWsB3IvCS13ZjkxEz1QjD7zPCaTT6ZG2bd/kOM5MIpqNQp0PmjdEtLu+vv7+wREG431LIB6P19u2fRuAdiKagcLJ10/+JIAfRqPRJ8Imrhcf2BzIZDIjiifgGyRJyjuOkxFCpIOuqh/i/xn/AwaAZSZq0Y26AAAAAElFTkSuQmCC"
                />
              </a>
            </li>
            <li>
              {" "}
              <SocialIcon
                style={{
                  height: "20px",
                  width: "20px",
                  marginLeft: "9px",
                  marginTop: "9px",
                }}
                url="https://www.linkedin.com"
                bgColor="#cccccc"
              />
            </li>
            <li>
              {" "}
              <SocialIcon
                style={{
                  height: "20px",
                  width: "20px",
                  marginLeft: "9px",
                  marginTop: "9px",
                }}
                url="https://www.instagram.com"
                bgColor="#cccccc"
              />
            </li>
            <li>
              {" "}
              <SocialIcon
                style={{
                  height: "20px",
                  width: "20px",
                  marginLeft: "9px",
                  marginTop: "9px",
                }}
                url="https://www.twitter.com"
                bgColor="#cccccc"
              />
            </li>
          </ul>
          <ul className="flex flex-col-reverse lg:flex-row lg:col-start-5  lg:col-end-8 text-right text-gray-400 text-sm font-thin h-5 pt-0.5  lg:justify-start ">
            <ul className="flex lg:col-start-6 lg:col-end-8 text-right text-gray-400 text-sm font-thin h-5 pt-0.5 justify-center  lg:justify-start mt-0.5 mb-2 lg:mb-0">
              <li className="border-r-2 px-2 .footer">
                <a href="#">بلاگ دیوار</a>
              </li>

              <li className="border-r-2 px-2 .footer">
                <a href="#">پشتیبانی و قوانین</a>
              </li>
              <li className=" px-2 .footer">
                <a href="#">درباره دیوار</a>
              </li>
            </ul>
            <ul className="flex lg:col-start-6 lg:col-end-9 text-right text-gray-400 text-3xl font-thin h-5  justify-center ml-6 mb-6">
              <li className=" pr-4 ">
                <a href="#">دیوار</a>
              </li>
            </ul>
          </ul>
        </ul>
      </footer>
      <footer className="w-full flex justify-center z-50 bg-white border-t-2 pt-2 pb-3  text-center md:invisible  visible static md:absolute  fixed inset-x-0 bottom-0">
        <button
          className="text-white text-base   rounded w-44 h-10 mr-3"
          style={{ backgroundColor: "#a62626" }}
        >
          اطلاعات تماس
        </button>
        <button
          className="text-white text-base rounded w-44 h-10"
          style={{ backgroundColor: "#a62626" }}
        >
          چت
        </button>
      </footer>
    </div>
  );
}
