module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, 
  corePlugins: {
     
    fontFamily: false,
    },
    
  theme: {
     extend: {
      fontFamily: {
        display: ["IRANSansXRegular"],
        body: ["IRANSansXBold"],
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
